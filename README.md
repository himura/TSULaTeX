# \LaTeX~-- светлая сторона Силы
### Использование системы компьютерной вёрстки LaTeX в учебной и научной деятельности

Тульский государственный университет, кафедра Приборы Управления.

---

### [Скачать книгу в PDF](main.pdf)


## Файлы и программы, необходимые для работы с LaTeX

* [MIKTeX](extra/basic-miktex-2.9.6069-x64.exe)
* [TeXstudio](extra/texstudio-2.11.2-win-qt5.6.1.exe)
* [Словарь для проверки орфографии в TeXstudio](extra/Spell Check)
* [Статья "Русский язык в LaTeX2e"](extra/RusInLaTeX.pdf)
* [PSCyr](extra/PSCyr)